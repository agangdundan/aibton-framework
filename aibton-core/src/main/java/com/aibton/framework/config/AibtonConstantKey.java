/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.config;

/**
 * 静态变量配置类
 * @author huzhihui
 * @version v 0.1 2017/5/11 22:03 huzhihui Exp $$
 */
public class AibtonConstantKey {

    /**
     * 系统正常返回code
     */
    public static final String RESPONSE_000000       = "000000";

    /**
     * 用户没有权限访问code
     */
    public static final String RESPONSE_400000       = "400000";

    /**
     * 系统内部异常
     */
    public static final String EXCEPTION_OF_MESSAGE  = "系统内部异常";

    /**
     * SYSTEM
     */
    public static final String SYSTEM                = "system";

    /**
     * JSON对象转换异常
     */
    public static final String SYSTEM_JACK_SON_ERROR = "JSON对象转换异常";

    /**
     * 用户没有权限访问该接口
     */
    public static final String USER_NOT_AUTH_ERROR   = "用户没有权限访问该接口";

    /**
     * HTTP请求调用异常
     */
    public static final String HTTP_ERROR            = "HTTP请求调用异常";

    /**
     * token值
     */
    public static final String TOKEN                 = "token";

    /**
     * session权限key
     */
    public static final String AIBTON_AUTH_DATAS     = "aibtonAuthDatas";
}
