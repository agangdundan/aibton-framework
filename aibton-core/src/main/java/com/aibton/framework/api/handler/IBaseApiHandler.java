package com.aibton.framework.api.handler;

import com.aibton.framework.api.data.EngineContext;

/**
 * Api基础处理接口
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/21 9:17 huzhihui Exp $$
 */
public interface IBaseApiHandler {

    /**
     * handel处理器主要方法
     * @param engineContext 处理器对象
     */
    void doHandel(EngineContext engineContext);
}
