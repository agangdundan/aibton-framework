package com.aibton.framework.api.auth.annotation;

import java.lang.annotation.*;

/**
 * 权限注解
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/28 10:38 huzhihui Exp $$
 */
@Retention(RetentionPolicy.RUNTIME) // 注解会在class字节码文件中存在，在运行时可以通过反射获取到
@Target({ ElementType.TYPE }) //定义注解的作用目标**作用范围字段、枚举的常量/方法
@Documented //说明该注解将被包含在javadoc中
public @interface Auth {

    /**
     * 需要的权限
     * @return  返回拥有的权限列表字符串
     */
    String[] auth();
}
